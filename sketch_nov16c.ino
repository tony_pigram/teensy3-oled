#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET  13
#define OLED_CS     12
#define OLED_DC     11
#define OLED_CLK    10
#define OLED_MOSI    9
#define OLED_CS_2    4
#define OLED_RESET_2 7
Adafruit_SSD1306 display1(OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);
Adafruit_SSD1306 display2(OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET_2, OLED_CS_2);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

uint8_t waterTemp = 0;
uint8_t fuelLevel = 100;

void setup()   {                
  Serial.begin(9600);
  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  display1.begin(SSD1306_SWITCHCAPVCC);
  display2.begin(SSD1306_SWITCHCAPVCC);
  // init done
  display1.display(); // show splashscreen on screen 1
  display2.display(); // show splashscreen on screen 2
  delay(2000);
  display1.clearDisplay();   // clears the screen and buffer
  display2.clearDisplay();   // clears the screen and buffer

  fuelLevel = 100;
  //draw the FUEL gauge
  drawFUEL();
  display2.display();
  delay(50);
  display2.clearDisplay();

  waterTemp = 0;
  //draw the WATER TEMP gauge
  drawWATERTEMP();  
  display1.display();
  delay(50);
  display1.clearDisplay();
  
  //draw the OIL TEMP gauge
  drawOILTEMP();
  delay(50);
}


void loop() {

  //read the WATER TEMP sensor
  //if changed then output the change to the correct display
//  display1.clearDisplay();
  if(waterTemp >= 100) { waterTemp = 100; }
  waterTemp = waterTemp+5;
  Serial.print("WaterTemp=");Serial.println(waterTemp);
  updateWATERTEMP();
  delay(1000);
  
  //read the FUEL sensor (probably do this once a minute? doesn't need to be that accurate!)
  //if changed then output the change to the correct display
//  display2.clearDisplay();
  if(fuelLevel <= 10) {
    fuelLevel = 0;
  } else {
    fuelLevel = fuelLevel - 5;
  }
  Serial.print("FuelLevel=");Serial.println(fuelLevel);
  updateFUEL();
  delay(1000);

  //read the OIL TEMP sensor
  //if changed then output the change to the correct display

}

void drawWATERTEMP(void) {
  display1.setTextSize(3);
  display1.setTextColor(WHITE);
  display1.setCursor(30,0);
  display1.clearDisplay();
  display1.println("TEMP");
  display1.drawRect(0, 30, display1.width(), 10, WHITE);
  display1.fillRect(0, 30, waterTemp, 10, 1);
}

void drawFUEL(void) {
  display2.setTextSize(3);
  display2.setTextColor(WHITE);
  display2.setCursor(30,0);
  display2.clearDisplay();
  display2.println("FUEL");
  display2.drawRect(0, 30, display2.width(), 10, WHITE);
  display2.fillRect(0, 30, fuelLevel, 10, 1);
}

void drawOILTEMP(void) {
}

void updateWATERTEMP(void) {
  if(waterTemp < 30) {
    drawWATERTEMP();
    display1.setTextSize(2);
    display1.setTextColor(WHITE);
    display1.setCursor(0,50);
    display1.println("COLD");
  } else if((waterTemp > 30) && (waterTemp < 70))
  {
    drawWATERTEMP();
    display1.setTextSize(2);
    display1.setTextColor(WHITE);
    display1.setCursor(30,50);
    display1.println("NORMAL");
  } else if(waterTemp > 70)
  {
    drawWATERTEMP();
    display1.setTextSize(2);
    display1.setTextColor(WHITE);
    display1.setCursor(80,50);
    display1.println("HOT!");
    display1.invertDisplay(true);
    delay(150);
    display1.invertDisplay(false);
    delay(150);
    display1.invertDisplay(true);
    delay(150);
    display1.invertDisplay(false);
    delay(150);
    display1.invertDisplay(true);
    delay(150);
    display1.invertDisplay(false);
  }
  display1.display();
  display1.clearDisplay();
}

void updateFUEL(void) {
  if(fuelLevel >= 30) {
      drawFUEL();
  } else if(fuelLevel != 0){
    drawFUEL();
    display2.setTextSize(2);
    display2.setTextColor(WHITE);
    display2.setCursor(20,50);
    display2.println("FUEL LOW");
  } else if(fuelLevel == 0) {
    display2.clearDisplay();
    display2.setTextSize(3);
    display2.setTextColor(WHITE);
    display2.setCursor(30,0);
    display2.clearDisplay();
    display2.println("FUEL");
    display2.setCursor(20,35);
    display2.println("EMPTY");
    display2.invertDisplay(true);
    delay(150);
    display2.invertDisplay(false);
    delay(150);
    display2.invertDisplay(true);
    delay(150);
    display2.invertDisplay(false);
    delay(150);
    display2.invertDisplay(true);
    delay(150);
    display2.invertDisplay(false);
  }
  display2.display();
  display2.clearDisplay();
}
